<head>
    <style>
        td, th {
            border: 1px solid black;
        }
        td.no-top {
            border-top-style: hidden;
        }
        td.no-bottom {
            border-bottom-style: hidden;
        }
        td.no-left {
            border-left-style: hidden;
        }
        td.no-right {
            border-right-style: hidden;
        }
        td.no-border {
            border-style: hidden;
        }
    </style>
</head>

<img src="assets/images/logobi.png">
<h2 style="text-align:center;">Laporan Tahunan Penggunaan ATK</h2>
<h3 style="text-align:center;">Permintaan ATK Terbanyak</h3>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <thead>
      <tr>
        <th>No.</th>
        <th>Nama ATK</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
        <?php $c = 1?>
        @foreach ($inventories as $inventory)
        <tr>
            <td>{{ $c++ }}</td>
            <td>{{ $inventory->name }}</td>
            <td>{{ $inventory->total }} {{ $inventory->unit }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
<h3 style="text-align:center;">Divisi dengan Permintaan Terbanyak</h3>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <thead>
      <tr>
        <th>No.</th>
        <th>Divisi</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      <?php $c = 1?>
        @foreach ($requests as $request)
        <tr>
            <td>{{ $c++ }}</td>
            <td>{{ $request->division }}</td>
            <td>{{ $request->total }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
<h3 style="text-align:center;">Permintaan Kertas</h3>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <thead>
      <tr>
        <th>No.</th>
        <th>Bulan</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      <?php $c = 1?>
        @foreach ($papers as $paper)
          <tr>
            <td>{{ $c++ }}</td>
            <td>{{ $paper->month }} {{ $paper->year }}</td>
            <td>{{ $paper->total }} {{ $paper->unit }}</td>
          </tr>
        @endforeach
    </tbody>
</table>
<br>
<h3 style="text-align:center;">Permintaan Toner</h3>
<table style="width:100%;text-align:center;border-collapse:collapse">
    <thead>
      <tr>
        <th>No.</th>
        <th>Bulan</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      <?php $c = 1?>
        @foreach ($toners as $toner)
        <tr>
            <td>{{ $c++ }}</td>
            <td>{{ $toner->month }} {{ $toner->year }}</td>
            <td>{{ $toner->total }} {{ $toner->unit }}</td>
        </tr>
        @endforeach
    </tbody>
</table>