@extends('layouts.admin')
@section('page-title', 'Kategori ATK')
@section('title', 'Kategori ATK')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('here', 'Kategori ATK')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    <a href="{{ route('cat-create') }}" class="btn waves-effect waves-light indigo darken-4 tooltipped" data-position="bottom" data-delay="1" data-tooltip="Tambah Kategori"><i class="mdi-content-add"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Daftar Kategori</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
            <div id="card-alert" class="card blue darken-1">
              <div class="card-content white-text darken-1">
                  <p class="single-alert">{{ session('flash_message') }}</p>
              </div>
            </div>
            <br>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th></th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th></th>
              </tr>
          </tfoot>
          <tbody>
            <?php $no = 1; ?>
            @foreach ($cats as $cat)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $cat->name }}</td>
                <td>{{ $cat->description ?: '-' }}</td>
                <td>
                  <a href="{{ route('cat-update', $cat->id) }}" class="btn waves-effect waves-light blue tooltipped" data-position="bottom" data-delay="1" data-tooltip="Ubah Kategori"><i class="mdi-editor-border-color"></i></a>
                  <a href="{{ route('cat-delete', $cat->id) }}" class="btn waves-effect waves-light light-blue darken-2 delete-category tooltipped" data-position="bottom" data-delay="1" data-tooltip="Hapus Kategori"><i class="mdi-content-clear"></i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
<script src="{{ asset('assets/js/jquery-validation.min.js') }}"></script>
<script>
  $("#formValidate").validate({
        rules: {
            name: {
              required: true,
            }
    },
        //For custom messages
        messages: {
            name: {
              required: "Nama kategori harus diisi",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
      $(placement).append(error);
      } else {
      error.insertAfter(element);
      }
        }
     });
</script>
@endsection