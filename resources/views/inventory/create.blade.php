@extends('layouts.admin')
@section('page-title', 'ATK Baru | ')
@section('title', 'ATK Baru')
@section('root') 
  <li><a class="blue-text text-darken-4" href="{{ route('index') }}">Dashboard</a></li>
@endsection
@section('previous')
  <li><a class="blue-text text-darken-4" href="{{ route('inventory-index') }}">Daftar ATK</a></li>
@endsection
@section('here', 'ATK Baru')
@if ($operator || $manager)
  @section('new_request', $count_requests)
@endif
@section('admin-content')
<div class="col s12 m8 l9">
	<div id="jqueryvalidation" class="section">
		<div class="container">
			<h4>Tambah ATK Baru</h4>
			<br>
			<div id="card-alert" class="card blue darken-1">
					<div class="card-content white-text darken-1">
						<p>Area dengan (*) wajib diisi.</p>
						@if (session()->has('flash_message'))
							<p class="single-alert">{{ session('flash_message') }}</p>
					    @endif
					</div>
            </div>
			<form id="formValidate" class="row formValidate" method="post" action="{{ route('inventory-create') }}" novalidate="novalidate">
				{{ csrf_field() }}
				<div class="input-field col s12 m12 l12 validate">
					<label for="name">Nama ATK*</label>
					<input type="text" name="name" data-error=".errorName">
					<div class="errorName"></div>
				</div>
				<div class="input-field col s12 m4 l4 validate">
					<label for="min_stock">Stok Minimal*</label>
					<input type="text" name="min_stock" data-error=".errorMinStock">
					<div class="errorMinStock"></div>
				</div>
				<div class="input-field col s12 m4 l4 unit">
					<!-- <label>Pilih Satuan</label> -->
					<select name="unit" data-error=".errorUnit" required="required">
					<option value="" disabled selected>Pilih Satuan*</option>
					@foreach ($units as $unit)
						<option value="{{ $unit->unit }}">{{ $unit->unit }}</option>
					@endforeach
					</select>
					<div class="errorUnit">
					</div>
				</div>
				<div class="input-field col s12 m4 l4 validate">
					<select name="category" data-error=".errorCategory" required="required" class="list-dropdown">
						<option value="" disabled selected>Pilih Kategori*</option>
						@foreach ($cats as $cat)
							<option value="{{ $cat->id }}">{{ $cat->name }}</option>
						@endforeach
					</select>
					<div class="errorCategory"></div>
				</div>
				<div class="col s12">
					<button class="btn waves-effect waves-light indigo darken-4 create-inventory">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('assets/js/jquery-validation.min.js') }}"></script>
<script>
    $("#formValidate").validate({
        rules: {
            name: {
                required: true,
            },
           	min_stock: {
                required: true,
                digits: true,
            },
            category: {
	            required: true,
            },
            'unit[]': {
            	required: true,
            }
		},
        //For custom messages
        messages: {
            name:{
                required: "Nama ATK harus diisi",
            },
            min_stock: {
            	required: "Stok minimal harus diisi",
            	digits: "Stok minimal harus berupa angka",
            },
            category: {
            	required: "Kategori harus dipilih",
            },
            'unit[]': {
            	required: "Pilih satuan jumlah",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection