@extends('layouts.main')

@section('content')
<div class="row" style="padding: 0 0 0 0;">
    <div class="col s12 m8 l8 slider" style="padding:0 0 0 0;height:100vh;">
        <div class="row" style="padding: 0 0 0 0;">
            <ul class="col s12 m12 slides" style="padding: 0 0 0 0;">
                <li>
                    <img src="assets/images/login.png" alt="" class="responsive-img">
                </li>
                <li>
                    <img src="assets/images/login1.png" alt="" class="responsive-img">
                </li>
                <li>
                    <img src="assets/images/login2.png" alt="" class="responsive-img">
                </li>
                <li>
                    <img src="assets/images/login3.png" alt="" class="responsive-img">
                </li>
            </ul>

            <ul id="card-about" class="col s12 m12" style="padding:7em 2em 5em 2em">
                    <div class="about1"><strong>SISTEM INFORMASI PENGELOLAAN ALAT TULIS KANTOR</strong></div>
                    <br>
                    <div class="about2">SIPATK adalah suatu sistem informasi yang berfungsi untuk pengelolaan ATK milik Departemen Penglolaan Sistem Informasi. Sistem yang dikelola oleh Satuan Layanan dan Administrasi ini diharapkan dapat mempermudah seluruh divisi di DPSI apabila ingin mengajukan permintaan ATK.</div>
            </ul>
        </div>
    </div>
    <div id="login-page jqueryvalidation" class="col s12 m4 l4" style="padding:0 0 0 0; margin: 0 0 0 0;">
        <div class="card-panel">
            <form id="formValidate" class="login-form formValidate" novalidate="novalidate" method="post" action="{{ route('login') }}" style="width:100%">
                {{ csrf_field() }}
                <div class="row" style="padding:0;height:40vh;border-left:6px solid blue">
                    <div class="input-field col s12 m12 center" style="padding:0;margin-top:7em;">
                        <img src="assets/images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
                        <p class="center login-form-text" style="font-size:1rem;">Sistem Informasi <br> Pengelolaan ATK - DPSI</p>
                    </div>
                </div>
                <div class="row" style="height:60vh;border-left:6px solid red;padding:2em 3em">
                    @if (session()->has('flash_message'))
                        <div id="card-alert" class="col s12 m12 card blue darken-1" style="padding:0 0 0 0;">
                            <div class="card-content white-text darken-1">
                                <p class="single-alert">{{ session('flash_message') }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="row margin" style="padding:0 0 0 0;">
                        <div class="input-field col s12 m12" style="padding:0 0 0 0;">
                            <i class="mdi-social-person-outline prefix"></i>
                            <label for="username" class="center-align">Username</label>
                            <input id="username" type="text" name="username" data-error=".errorUsername">
                            <div class="errorUsername" style="margin-left:3em;"></div>
                        </div>
                    </div>
                    <div class="row margin" style="padding:0 0 0 0;">
                        <div class="input-field col s12 m12 validate" style="padding:0 0 0 0;">
                            <i class="mdi-action-lock-outline prefix"></i>
                            <label for="password">Password</label>
                            <input id="password" type="password" name="password" data-error=".errorPassword">
                            <div class="errorPassword" style="margin-left:3em;"></div>
                        </div>
                    </div>
                    <div class="row buttonLogin">
                        <div class="input-field col s12 m12 validate" style="padding:0 0 0 0;">
                            <button class="btn waves-effect waves-light col s12 indigo darken-4">Login</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="jquery.min.js"></script>
<script src="jssor.slider.mini.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        //For custom messages
        messages: {
            username:{
                required: "Username harus diisi",
            },
            password: {
                required: "Password harus diisi",
            },
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
    });
    $(document).ready(function(){
        $('.slider').slider({
        height:70vh;
        });
        // Pause slider
        $('.slider').slider('pause');
        // Start slider
        $('.slider').slider('start');
        // Next slide
        $('.slider').slider('next');
        // Previous slide
        $('.slider').slider('prev');
    });
</script>
@endsection