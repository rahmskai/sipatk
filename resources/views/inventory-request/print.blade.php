<div>
    <img src="assets/images/logobi.png">
    <p style="margin-top:-40px;text-align:right;font-weight:bold">Satuan Layanan Administrasi</p>
    <br>
    <br>
    <h2 style="text-align:center;margin-bottom:0">Bon Permintaan ATK {{ date_format($request->created_at, 'd-m-Y') }}</h2>
    <h5 style="text-align:center;margin-top:0">
        @if ($request->status == 2)
            Permintaan ini telah disetujui oleh {{ $request->u_approver->name }}.
        @elseif ($request->status == 3)
            Permintaan ini telah disetujui oleh SLA.
        @elseif ($request->status == 4)
            Permintaan ini sedang diproses.
        @elseif ($request->status == 5)
            Permintaan ini telah selesai diproses.
        @elseif ($request->status == 0 && $request->approvedByDiv == 0)
            Permintaan ini ditolak oleh kepala divisi.
        @elseif ($request->status == 0)
            Permintaan ini ditolak oleh SLA.
        @else
            Sedang menanti persetujuan {{ $request->u_approver->name }}.
        @endif
    </h5>
    <table>
        <tr>
            <td><b>Permintaan oleh</b></td>
            <td><b>:</b></td>
            <td>{{ $request->u_sender->name }} / {{ $request->u_sender->division }}</td>
        </tr>
        <tr>
            <td><b>ATK yang diminta</b></td>
            <td><b>:</b></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
            @foreach ($inventories as $inventory)
              - {{ $inventory->inventory->name }} / {{ $inventory->quantity }} {{ $inventory->unit }} <br>
            @endforeach
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><b>Untuk keperluan</b></td>
            <td><b>:</b></td>
            <td>{{ $request->purpose }}</td>
        </tr>
        @if ($request->admin_note != '')
            <tr>
                <td><b>Catatan</b></td>
                <td><b>:</b></td>
                <td>{{ $request->admin_note }}</td>
            </tr>
        @endif
    </table>
    <div>
        <br>
        @if ($request->status == 2)
        --- <br>
            Disetujui oleh Kepala Divisi ({{ $request->u_approver->name }}).
        @elseif ($request->status == 3)
        --- <br>
            Disetujui oleh Kepala Divisi ({{ $request->u_approver->name }}). <br>
            Disetujui oleh SLA.
        @endif
    </div>
</div>