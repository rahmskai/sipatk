<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $primaryKey = 'username';
    public $incrementing = false;

    protected $casts = [
        'isOperator' => 'boolean',
        'isManager' => 'boolean',
        'isShown' => 'boolean'
    ];

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function requests()
    {
        return $this->hasMany(InventoryRequest::class);
    }

    public function getRememberToken()
     {
       return null; // not supported
     }

     public function setRememberToken($value)
     {
       // not supported
     }

     public function getRememberTokenName()
     {
       return null; // not supported
     }

     /**
      * Overrides the method to ignore the remember token.
      */
     public function setAttribute($key, $value)
     {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
     }
}
