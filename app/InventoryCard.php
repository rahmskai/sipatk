<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCard extends Model
{
    protected $table = 'inventory_cards';
    public $incrementing = false;

    protected $fillable = [
        'inventory_id',
        'transaction_date',
        'bill_num',
        'from',
        'to',
        'in',
        'out',
        'stock',
        'isShown',
    ];

    public function inventory() 
    {
    	return $this->belongsTo(Inventory::class, 'inventory_id');
    }
}
