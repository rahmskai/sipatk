<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinInventoryCategory extends Model
{
    protected $table = 'bin_inventory_categories';
    public $incrementing = false;

    public function category()
    {
        return $this->belongsTo(InventoryCategory::class);
    }
}
