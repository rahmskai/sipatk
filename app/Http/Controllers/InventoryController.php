<?php

namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\BinInventory;
use App\Inventory;
use App\InventoryCard;
use App\InventoryCategory;
use App\InventoryRequest;
use App\UnitQuantity;
use App\User;
use Carbon\Carbon;
use DB;

class InventoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');

        $whoami = whoami();
        View::share('whoami', $whoami);

        $operator = operator();
        View::share('operator', $operator);

        $manager = manager();
        View::share('manager', $manager);

        $count_requests = count_requests();
        View::share('count_requests', $count_requests);
    }

    public function index()
    {
        if (operator() || manager() && (session()->get('username') == 'baharipri' || session()->get('username') == 'marta')) {
            $inventories = Inventory::where('isShown', 1)
                            ->orderBy('name', 'asc')
                            ->with('cards')
                            ->with('category')
                            ->get();

            // hack to update status for harcoded transactions
            // foreach ($inventories as $inventory) {
            //     set_inventoryStatus($inventory->cards->last(), $inventory);
            // }
                            
            return view('inventory.index', compact('inventories'));
        }
        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function create()
    {
        if (operator()) {
            $units = UnitQuantity::all();
            $cats = InventoryCategory::where('isShown', 1)->get();

        	return view('inventory.create', compact('units', 'cats'));            
        } 

        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'min_stock' => 'required|integer',
            'category' => 'required|exists:inventory_categories,id',
            'unit' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('flash_message', 'Ada kesalahan input.');
            return redirect()->route('inventory-create')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $input = $request->all();
            $input['status'] = 0;
            $input['isShown'] = 1;

            if(Inventory::where('name', $input['name'])->exists()) {
                $inventory = Inventory::where('name', $input['name'])->first();

                Inventory::where('name', $input['name'])->update([
                    'name' => $input['name'],
                    'min_stock' => $input['min_stock'],
                    'category' => $input['category'],
                    'unit' => $input['unit'],
                    'isShown' => $input['isShown'],
                ]);

                new_binInventory($inventory);

                session()->flash('flash_message', 'ATK '.$input['name'].' kembali diadakan.');
            } else {
                $inventory = Inventory::create([
                    'name' => $input['name'],
                    'min_stock' => $input['min_stock'],
                    'category' => $input['category'],
                    'unit' => $input['unit'],
                    'status' => $input['status'],
                    'isShown' => $input['isShown'],
                ]); 

                //hack to add input value
                Inventory::where('name', $input['name'])->update([
                    'unit' => $input['unit'],
                ]);

                // Hack to get the last inserted inventory
                $newInventory = Inventory::get()->last();

                $newInventory->cards()->create([
                    'bill_num' => '-',
                    'from' => '-',
                    'to' => '-',
                    'in' => 0,
                    'out' => 0,
                    'stock' => 0,
                    'isShown' => 0,
                ]);

                session()->flash('flash_message', 'Terima kasih, '.$newInventory->name.' telah dibuat.');
            }
            return redirect()->route('inventory-index');
        }
    }

    public function update($id)
    {
        if (operator()) {
            $inventory = Inventory::findOrFail($id);
            $units = UnitQuantity::all();
            $cats = InventoryCategory::where('isShown', 1)->get();
            
            return view('inventory.update', compact('inventory', 'units', 'cats'));
        }

        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function storeUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'min_stock' => 'required|integer',
            'category' => 'required|exists:inventory_categories,id',
            'unit' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('barang/'.$id.'/ubah')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $inventory = Inventory::find($id);

            $input = $request->all();
            Inventory::where('id', $id)->update([
                    'name' => $input['name'],
                    'min_stock' => $input['min_stock'],
                    'unit' => $input['unit'],
                    'category' => $input['category'],
            ]);

            new_binInventory($inventory);

            $updated_inventory = Inventory::find($id);
            set_inventoryStatus($inventory->cards()->orderBy('created_at', 'desc')->first(), $updated_inventory);
            
            session()->flash('flash_message', $inventory->name.' berhasil diubah.');
            return redirect()->route('inventory-index');
        }
    }

    public function delete(Request $request, $id)
    {
        if (operator()) {
            $inventory = Inventory::find($id);

            Inventory::where('id', $id)->update([
                    'isShown' => 0,
                ]);
            new_binInventory($inventory);

            session()->flash('flash_message', $inventory->name.' berhasil dihapus.');
            return redirect()->route('inventory-index');
        }

        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function exportRecap()
    {
        $years = DB::select(DB::raw('select distinct year(transaction_date) as `year` from inventory_cards where isShown = 1'));
        if (!is_null(InventoryCard::where('isShown', 1)->get())) {
            \Excel::create('Rekapitulasi ATK', function($excel) use($years) {
                foreach ($years as $year) {
                    $inventories = Inventory::where('isShown', 1)
                                    ->orderBy('name', 'asc')
                                    ->with(['cards' => function($q) use($year) {
                                        $q->where('isShown', 1)
                                            ->whereYear('transaction_date', '=', $year->year)
                                            ->orderBy('transaction_date', 'asc');
                                    }])->get();
                    $latest_transactions = Inventory::where('isShown', 1)
                                        ->with(['cards' => function($q) use($year) {
                                            $q->where('isShown', 1)
                                                ->whereYear('transaction_date', '=', $year->year - 1)
                                                ->orderBy('transaction_date', 'desc');
                                        }])->get();
                    $transactions = InventoryCard::where('isShown', 1)
                                    ->whereYear('transaction_date', '=', $year->year)
                                    ->orderBy('transaction_date', 'asc')
                                    ->get();
                    $excel->sheet('Tahun '.$year->year, function($sheet) use ($year, $latest_transactions, $transactions, $inventories) {
                        $sheet->setOrientation('landscape');
                        $sheet->setAutoSize(true);
                        $sheet->prependRow(1, array(
                            'Rekapitulasi Penerimaan, Pengeluaran, dan Persediaan'
                        ));
                        $sheet->prependRow(2, array(
                            'Alat Tulis Kantor (ATK) bagian AdTI'
                        ));
                        $sheet->prependRow(3, array(
                            'per TA.18 '.Carbon::createFromFormat('Y-m-d', $transactions->first()->transaction_date)->format('d M Y').' s/d '.Carbon::createFromFormat('Y-m-d', $transactions->last()->transaction_date)->format('d M Y'),
                        ));
                        $sheet->prependRow(5, array(
                            'No', 
                            'Nama Barang',
                            'Stok',
                            'Jan',
                            '',
                            'Feb',
                            '',
                            'Mar',
                            '',
                            'Apr',
                            '',
                            'Mei',
                            '',
                            'Jun',
                            '',
                            'Jul',
                            '',
                            'Ags',
                            '',
                            'Sep',
                            '',
                            'Okt',
                            '',
                            'Nov',
                            '',
                            'Des',
                            '',
                            'Jumlah',
                            '',
                            'Stock',
                        ));
                        $sheet->prependRow(6, array(
                            '',
                            '',
                            Carbon::createFromFormat('Y-m-d', $transactions->first()->transaction_date)->format('d M'),
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            'M',
                            'K',
                            Carbon::createFromFormat('Y-m-d', $transactions->last()->transaction_date)->format('d M'),
                        ));
                        $sheet->mergeCells('A1:AD1');
                        $sheet->mergeCells('A2:AD2');
                        $sheet->mergeCells('A3:AD3');
                        $sheet->setMergeColumn(array(
                            'columns' => array('A'),
                            'rows' => array(
                                array(5,6),
                            )
                        ));
                        $sheet->setMergeColumn(array(
                            'columns' => array('B'),
                            'rows' => array(
                                array(5,6),
                            )
                        ));
                        $sheet->mergeCells('D5:E5');
                        $sheet->mergeCells('F5:G5');
                        $sheet->mergeCells('H5:I5');
                        $sheet->mergeCells('J5:K5');
                        $sheet->mergeCells('L5:M5');
                        $sheet->mergeCells('N5:O5');
                        $sheet->mergeCells('N5:O5');
                        $sheet->mergeCells('P5:Q5');
                        $sheet->mergeCells('R5:S5');
                        $sheet->mergeCells('T5:U5');
                        $sheet->mergeCells('V5:W5');
                        $sheet->mergeCells('X5:Y5');
                        $sheet->mergeCells('Z5:AA5');
                        $sheet->mergeCells('AB5:AC5');
                        
                        $no = 1;
                        $first_stock = 0;
                        $last_stock = 0;
                        foreach ($inventories as $inventory) {
                            if (!is_null($latest_transactions->find($no)) && !is_null($latest_transactions->find($no)->cards->first()))
                                $first_stock = $latest_transactions->find($no)->cards->first()->stock;
                            else
                                $first_stock = 0;
                            if (!is_null($latest_transactions->find($no)) && !is_null($inventories->find($no)->cards->first()))
                                $last_stock = $inventories->find($no)->cards->last()->stock;
                            else
                                $last_stock = 0;
                            
                            $sheet->appendRow([
                                $no,
                                $inventory->name,
                                $first_stock,
                                // jan
                                in_per_month(1, $year->year, $no) ?: '',
                                out_per_month(1, $year->year, $no) ?: '',
                                // feb
                                in_per_month(2, $year->year, $no) ?: '',
                                out_per_month(2, $year->year, $no) ?: '',
                                // mar
                                in_per_month(3, $year->year, $no) ?: '',
                                out_per_month(3, $year->year, $no) ?: '',
                                // apr
                                in_per_month(4, $year->year, $no) ?: '',
                                out_per_month(4, $year->year, $no) ?: '',
                                // mei
                                in_per_month(5, $year->year, $no) ?: '',
                                out_per_month(5, $year->year, $no) ?: '',
                                // jun
                                in_per_month(6, $year->year, $no) ?: '',
                                out_per_month(6, $year->year, $no) ?: '',
                                // jul
                                in_per_month(7, $year->year, $no) ?: '',
                                out_per_month(7, $year->year, $no) ?: '',
                                // ags
                                in_per_month(8, $year->year, $no) ?: '',
                                out_per_month(8, $year->year, $no) ?: '',
                                // sep
                                in_per_month(9, $year->year, $no) ?: '',
                                out_per_month(9, $year->year, $no) ?: '',
                                // okt
                                in_per_month(10, $year->year, $no) ?: '',
                                out_per_month(10, $year->year, $no) ?: '',
                                // nov
                                in_per_month(11, $year->year, $no) ?: '',
                                out_per_month(11, $year->year, $no) ?: '',
                                // des
                                in_per_month(12, $year->year, $no) ?: '',
                                out_per_month(12, $year->year, $no) ?: '',
                                // jumlah
                                in_total($year->year, $no),
                                out_total($year->year, $no),
                                $last_stock,
                            ]);
                            $no++;
                        }
                        $sheet->cells('A1:AD'.$sheet->getHighestRow(), function($cells) {
                            $cells->setAlignment('center')
                                    ->setValignment('center')
                                    ->setBorder('solid', 'solid', 'solid', 'solid');
                        });
                        $sheet->row(1, function ($row) {
                            $row->setFontWeight('bold');
                        });
                        $sheet->row(2, function ($row) {
                            $row->setFontWeight('bold');
                        });
                        $sheet->row(3, function ($row) {
                            $row->setFontWeight('bold');
                        });
                        $sheet->row(5, function ($row) {
                            $row->setFontWeight('bold');
                        });
                        $sheet->cells('C7:C'.$sheet->getHighestRow(), function($cells) {
                            $cells->setBackground('#ffff00');
                        });
                        $sheet->cells('AD7:AD'.$sheet->getHighestRow(), function($cells) {
                            $cells->setBackground('#ffff00');
                        });
                        $sheet->cells('AB7:AB'.$sheet->getHighestRow(), function($cells) {
                            $cells->setBackground('#00ffff');
                        });
                        $sheet->cells('AC7:AC'.$sheet->getHighestRow(), function($cells) {
                            $cells->setBackground('#00ffff');
                        });
                        $sheet->setBorder('A5:AD'.$sheet->getHighestRow(), 'thin');
                        $sheet->getStyle('A7:AD'.$sheet->getHighestRow())->getAlignment()->setWrapText(true);
                    });
                }
            })->export('xls');
        } else {
           session()->flash('flash_message', 'Belum ada transaksi.');
           return redirect()->route('inventory-index');
        }
    }
}
