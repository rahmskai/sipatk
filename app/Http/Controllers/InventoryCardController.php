<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Validator;
use DateTime;
use App\Http\Requests;
use App\Inventory;
use App\InventoryCard;
use App\InventoryRequest;
use App\User;
use Carbon\Carbon;
use PDF;
use DB;

class InventoryCardController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');

        $whoami = whoami();
        View::share('whoami', $whoami);

        $operator = operator();
        View::share('operator', $operator);

        $manager = manager();
        View::share('manager', $manager);

        $count_requests = count_requests();
        View::share('count_requests', $count_requests);
    }

    public function card($id)
    {
        if (operator() || manager() && (session()->get('username') ==  'marta' || session()->get('username') == 'baharipri')) {
            $inventory = Inventory::findOrFail($id);
            $card = $inventory->cards()->where('isShown', 1)->orderBy('transaction_date', 'asc')->get();
            
            return view('inventory-card.index', compact('card', 'inventory'));
        }
        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function create($id)
    {
        if (operator()) {
    	$inventory = Inventory::findOrFail($id);

    	return view('inventory-card.create', compact('inventory'));
        }

        session()->flash('flash_message', 'Anda tidak memiliki akses untuk halaman tersebut.');
        return redirect()->route('request-index-user', session()->get('username'));
    }

    public function store(Request $request, $id)
    {
    	$input = $request->all();
        $validator = Validator::make($request->all(), [
            'transaction_date' => 'required|date_format:d/m/Y',
            'in' => 'integer',
            'out' => 'integer',
        ]);

        if ($validator->fails()) {
            return redirect()->route('card-create', $id)
                    ->withErrors($validator)
                    ->withInput();
        } else {
            if ($input['bill_num'] == "Permintaan") {
                session()->flash('flash_message', 'Memasukkan transaksi "Permintaan" secara manual tidak diizinkan.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['in'] == "" && $input['out'] == "") {
                session()->flash('flash_message', 'Mohon masukkan data transaksi masuk/keluar.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['in'] != "" && $input['out'] != "") {
                session()->flash('flash_message', 'Satu transaksi hanya dapat berisi transaksi masuk/keluar saja.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['in'] != "" && $input['from'] == "") {
                session()->flash('flash_message', 'Jumlah masuk hanya dapat diisi jika asal barang terisi.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['out'] != "" && $input['to'] == "") {
                session()->flash('flash_message', 'Jumlah keluar hanya dapat diisi jika tujuan barang terisi.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else {
                $inventory = Inventory::find($id);

                $input['inventory_id'] = $id;
                $t_date = DateTime::createFromFormat('d/m/Y', $input['transaction_date'])->format('Y-m-d');
                $input['transaction_date'] = $t_date;
                if ($input['from'] == "")
                    $input['from'] = "-";
                if ($input['to'] == "")
                    $input['to'] = "-";
                if ($input['in'] == "")
                    $input['in'] = 0;
                if ($input['out'] == "")
                    $input['out'] = 0;
                $input['isShown'] = 1;
                $input['stock'] = 0;
                InventoryCard::create($input);

                get_stock($inventory->cards()->where('isShown', 1)->orderBy('transaction_date', 'asc')->get());

                set_inventoryStatus($inventory->cards()->orderBy('created_at', 'desc')->first(), $inventory);

                session()->flash('flash_message', 'Terima kasih, transaksi baru pada tanggal '.$input['transaction_date'].' berhasil dimasukkan.');
            	return redirect()->route('card-detail', $id);   
            }
        }
    }

    public function export($id)
    {
        $inventory = Inventory::findOrFail($id);
        $year = Carbon::now()->year;
        $transactions = $inventory->cards()->where('isShown', 1)->where(DB::raw('year(created_at)'), '=', DB::raw('year(now())'))->orderBy('transaction_date', 'asc')->get();
        
        $pdf = PDF::loadView('inventory-card.print', compact('transactions', 'inventory', 'year'));
        return $pdf->download('Kartu Barang - '.$inventory->name.'.pdf');
   }
}
