<?php

	Route::group(['prefix' => '/'], function() {
		Route::get('/', [
			'as' => 'index',
			'uses' => 'DashboardController@index',
		]);
		Route::get('cetak', [
			'as' => 'dashboard-export',
			'uses' => 'DashboardController@export',
		]);
	});

	Route::get('bantuan', [
		'as' => 'help',
		'uses' => 'DashboardController@help',
	]);

	Route::get('test', function() {
	    return view('ldap-test');
	});

	Route::group(['middleware' => ['web']], function () {
		Route::group(['prefix' => 'login'], function() {
			Route::get('/', [
				'as' => 'login',
				'uses' => 'Auth\AuthController@loginForm',
			]);
			Route::post('/', 'Auth\AuthController@authenticate');
		});

		Route::get('logout', [
			'as' => 'logout',
			'uses' => 'Auth\AuthController@logout'
		]);
	});

	Route::group(['prefix' => 'barang'], function() {
		Route::get('/', [
			'as' => 'inventory-index',
			'uses' => 'InventoryController@index',
		]);
		Route::get('buat', [
			'as' => 'inventory-create',
			'uses' => 'InventoryController@create',
		]);
		Route::post('buat', 'InventoryController@store');
		Route::get('rekap', [
			'as' => 'inventory-recap',
			'uses' => 'InventoryController@exportRecap',
		]);
		Route::get('{id}/hapus', [
			'as' => 'inventory-delete',
			'uses' => 'InventoryController@delete',
		]);
		Route::get('{id}/ubah', [
			'as' => 'inventory-update',
			'uses' => 'InventoryController@update',
		]);
		Route::post('{id}/ubah', 'InventoryController@storeUpdate');
	});

	Route::group(['prefix' => 'kartu'], function() {
		Route::get('/', function() {
			return redirect()->route('inventory-index');
		});
		Route::get('{id}/cetak', [
			'as' => 'card-export',
			'uses' => 'InventoryCardController@export',
		]);
		Route::get('{id}/buat', [
			'as' => 'card-create',
			'uses' => 'InventoryCardController@create',
		]);
		Route::post('{id}/buat', 'InventoryCardController@store');
		Route::get('{id}', [
			'as' => 'card-detail',
			'uses' => 'InventoryCardController@card'
		]);
	});

	Route::group(['prefix' => 'kategori'], function() {
		Route::get('/', [
			'as' => 'cat-index',
			'uses' => 'InventoryCategoryController@index',
		]);
		Route::get('buat', [
			'as' => 'cat-create',
			'uses' => 'InventoryCategoryController@create',
		]);
		Route::post('buat', 'InventoryCategoryController@store');
		Route::get('{id}/ubah', [
			'as' => 'cat-update',
			'uses' => 'InventoryCategoryController@update',
		]);
		Route::post('{id}/ubah', 'InventoryCategoryController@storeUpdate');
		Route::get('{id}/hapus', [
				'as' => 'cat-delete',
				'uses' => 'InventoryCategoryController@delete',
			]);
	});

	Route::group(['prefix' => 'permintaan'], function() {
		Route::get('/', [
				'as' => 'request-index',
				'uses' => 'RequestController@index',
			]);
		Route::get('user/{user}', [
				'as' => 'request-index-user',
				'uses' => 'RequestController@user',
			]);
		Route::get('buat', [
				'as' => 'request-create',
				'uses' => 'RequestController@create',
			]);
		Route::post('buat', 'RequestController@store');
		Route::get('{id}/ubah', [
				'as' => 'request-update',
				'uses' => 'RequestController@update',
			]);
		Route::post('{id}/ubah', 'RequestController@storeUpdate');
		Route::get('{id}/hapus', [
				'as' => 'request-delete',
				'uses' => 'RequestController@delete',
			]);
		Route::get('{id}/terima-oleh-divisi', [
				'as' => 'request-approve-div',
				'uses' => 'RequestController@approveByDiv',
		]);
		Route::post('{id}/tolak-oleh-divisi', [
				'as' => 'request-reject-div',
				'uses' => 'RequestController@rejectByDiv',
		]);
		Route::get('{id}/terima-oleh-sla', [
				'as' => 'request-approve-sla',
				'uses' => 'RequestController@approveBySLA',
		]);
		Route::post('{id}/tolak-oleh-sla', [
				'as' => 'request-reject-sla',
				'uses' => 'RequestController@rejectBySLA',
		]);
		Route::get('{id}/proses', [
				'as' => 'request-process',
				'uses' => 'RequestController@process',
		]);
		Route::post('{id}/selesai', [
				'as' => 'request-finish',
				'uses' => 'RequestController@finish',
		]);
        Route::get('{id}/cetak', [
				'as' => 'request-export',
				'uses' => 'RequestController@export',
		]);
		Route::get('{id}', [
				'as' => 'request-detail',
				'uses' => 'RequestController@detail',
        ]);
	});

    Route::group(['prefix' => 'admin'], function() {
        Route::get('/', [
            'as' => 'admin-index',
            'uses' => 'AdminController@index',
        ]);
        Route::get('tambah', [
            'as' => 'admin-create',
            'uses' => 'AdminController@create',
        ]);
        Route::post('tambah', 'AdminController@store');
        Route::get('{id}/hapus', [
            'as' => 'admin-delete',
            'uses' => 'AdminController@delete',
        ]);
        Route::get('{id}', [
            'as' => 'admin-update',
            'uses' => 'AdminController@update',
        ]);
        Route::post('{id}', 'AdminController@storeUpdate');
    });
// Route::auth();
