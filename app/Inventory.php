<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    public $incrementing = false;

    /* add and update data */
    protected $fillable = [
        'name',
        'min_stock',
        'category',
        'status',
        'isShown',
    ];

    public function category()
    {
        return $this->belongsTo(InventoryCategory::class, 'category');
    }

    public function cards()
    {
        return $this->hasMany(InventoryCard::class);
    }

    public function requests()
    {
        return $this->hasMany(RequestedInventory::class);
    }

    public function bins()
    {
        return $this->hasMany(BinInventory::class);
    }
}
