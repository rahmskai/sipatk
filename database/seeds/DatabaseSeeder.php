<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(InventoryCategorySeeder::class);
        $this->call(InventorySeeder::class);
        $this->call(InventoryCardSeeder::class);
        $this->call(RequestSeeder::class);
        $this->call(RequestedInventorySeeder::class);
        $this->call(UnitQuantitySeeder::class);
    }
}
