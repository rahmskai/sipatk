<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinRequestedInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_requested_inventories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('id')->unsigned();
            $table->integer('request_id');
            $table->integer('inventory_id');
            $table->integer('quantity');
            $table->string('unit');
            $table->timestamps();

            $table->foreign('id')
                    ->references('id')
                    ->on('requested_inventories')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_requested_inventories');
    }
}