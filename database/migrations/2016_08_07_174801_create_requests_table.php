<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('author');
            $table->string('status');
            $table->text('purpose');
            $table->string('approver');
            $table->string('receiver')->nullable();
            $table->string('admin_note')->nullable();
            $table->string('ticket')->nullable();
            $table->boolean('approvedByDiv');
            $table->boolean('isShown');
            $table->timestamps();

            $table->foreign('author')
                    ->references('username')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('receiver')
                    ->references('username')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('approver')
                    ->references('username')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
    }
}
