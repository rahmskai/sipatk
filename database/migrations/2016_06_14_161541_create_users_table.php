<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->string('username')->unique();
            $table->string('nip');
            $table->string('email');
            $table->string('name');
            $table->string('division');
            $table->boolean('isOperator');
            $table->boolean('isManager');
            $table->boolean('isShown');
            $table->timestamps();

           // $table->foreign('division_id')
           //         ->references('id')
           //         ->on('divisions')
           //         ->onUpdate('cascade')
           //         ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
