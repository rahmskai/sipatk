<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinInventoryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_inventory_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();

            $table->foreign('id')
                ->references('id')
                ->on('inventory_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->index([
                'id',
                'updated_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_inventory_categories');
    }
}
